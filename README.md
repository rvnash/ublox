# Description of this Library #

The library's purpose is to give an efficient and simple interface to most
U-Blox GPS chips. It turns off the NMEA reports and uses the UBX protocol
exclusively.

# U-BLOX GPS Systems #
I tested this using the CAM-M8Q U-Blox product. I suspect it will work with
others that follow the M8 Protocol.

See this protocol manual: https://www.u-blox.com/en/docs/UBX-13003221

# How to use the library #
See the example code and read the comments in src/ubloxGPS.h. This should guide
you through using this code successfully. In addition look at the example
code in "examples/usage/UBLOX_example.cpp". It's pretty straight forward to use,
which is the point of the library.
