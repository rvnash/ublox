EXE=UBLOX_example.bin
# TARGET=electron
TARGET=photon
TARGET_VERSION=1.5.0
BINDIR=bin

$(BINDIR)/$(EXE): src/*.cpp src/*.h examples/usage/*.cpp
	mkdir -p $(BINDIR)
	particle compile --target $(TARGET_VERSION) --saveTo $(BINDIR)/$(EXE) $(TARGET) project.properties src examples/usage

flash: $(BINDIR)/$(EXE)
	particle flash --usb $(BINDIR)/$(EXE)

clean:
	rm -rf $(BINDIR) *~ src/*~ examples/usage/*~
