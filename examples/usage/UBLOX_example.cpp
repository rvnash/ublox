/*
  usage.ino - Example usage for APRS library by Richard Nash.

Copyright (C) 2020 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Particle.h"
SYSTEM_MODE(MANUAL);
#include "ubloxGPS.h"

ubloxGPS gps;

void button_handler(system_event_t event, int duration, void* ){if (duration > 0) System.dfu();}

void setup()
{
  Serial.begin();
  System.on(button_status, button_handler);
  bool gpsSetupSucceeded = gps.init(&Serial1, 9600, 1000, true);
  if (!gpsSetupSucceeded) {
    Serial.printf("\nGPS setup FAILED\n");
  }
}

void printSatellites(uint8_t numSats, SatelliteInfo *sats)
{
  for (uint8_t i = 0; i < numSats; i++) {
    Serial.printf( "  GNSS: " );
    switch (sats[i].gnssId) {
      case 0:
        Serial.printf("GPS    ");
      break;
      case 1:
        Serial.printf("SBAS   ");
      break;
      case 2:
        Serial.printf("Galileo");
      break;
      case 3:
        Serial.printf("BeiDou ");
      break;
      case 4:
        Serial.printf("IMES   ");
      break;
      case 5:
        Serial.printf("QZSS   ");
      break;
      case 6:
        Serial.printf("GLONASS");
      break;
    }
    Serial.printf( " Num: %3d, Elevation: ", sats[i].satNum);
    if (sats[i].elev >= -90 && sats[i].elev <= 90 ) {
      Serial.printf("%3d°, Azimuth: %3d°", sats[i].elev, sats[i].azim);
    } else {
      Serial.printf(" ?? , Azimuth:  ?? ");
    }
    Serial.printf( ", Signal: %02d dBHz", sats[i].cno);
    Serial.printf( ", Used: %c\n", (sats[i].flags & 0x8) ? 'Y' : 'N');
  }
}

void printAllSatellites()
{
  Serial.printf("\nSatellites (%d)\n", gps.numberOfSatellites);
  Serial.printf(  "------------------\n");
  printSatellites(gps.numberOfSatellites,gps.sats);
  Serial.printf("\n");
}

void printStatus()
{
  printAllSatellites();
  Serial.printf("\n%d/%d/%d %02d:%02d:%02d - ",
                Time.month(), Time.day(), Time.year(), Time.hour(), Time.minute(), Time.second());
  if (gps.fixValid == FixValidity::Fix3D) {
    Serial.printf("%lf, %lf, Acc %4.1lf/%4.1lf, Used: %d/%d, alt: %lf, rate: %lf, head: %lf, speed: %lf\n",
                  gps.latitudeDegrees, gps.longitudeDegrees,
                  gps.positionAccuracyMeters, gps.altitudeAccuracyMeters,
                  gps.numSatellitesUsed, gps.numberOfSatellites,
                  gps.altitudeMeters,
                  gps.climbSpeedMetersPerSecond,
                  gps.headingDegrees,
                  gps.overGroundSpeedMetersPerSecond);
  } else {
    Serial.printf("No fix, Sats: %d\n", gps.numberOfSatellites );
  }
}

static int lastPrintTime = 0;
void loop()
{
  gps.loop();
  if (gps.lastPollTimeJump > 10) {
    Serial.printf("Time jump\n");
    lastPrintTime = 0;
    gps.lastPollTimeJump = 0; // I took care of the time jump
  }
  if (millis() > lastPrintTime+1000) {
    printStatus();
    lastPrintTime = millis();
  }
}
