/*
  ubloxGPS - Simplified interface to U-Blox M8 style GPS system

  The key feature is this turns off the NMEA style GPS and uses the
  smaller and more efficient UBX messages. The interface is simplified
  to a few commands to init, pump from the loop(), and grab results
  directly from public variables.

  Note: An instance of this class takes up approximately 430 bytes of RAM the way it
  is configured below. Also, the ublox_protocol instance which this class uses takes
  another 525 bytes. So it is getting up close to 1K of RAM.
  If this is too much for your hardware, then lower MAX_SATELLITES and/or MAX_PAYLOAD_LENGTH

  Note: The accomanying "ublox_protocol" class is a helper class and a more direct implementation
  of the ublox_protocol. But it is incomplete, and less well documented than this class. If you
  wish to explore using it directly, feel free to do so, but that is not the intention of this
  library.

  Copyright (c) 2020 - Richard Nash
*/

#pragma once

#include "ublox_protocol.h"
#include <Particle.h>

enum FixValidity
{
  FixNo, Fix2D, Fix3D
};

struct SatelliteInfo
{
  uint8_t gnssId; // GPS=0, SBAS=1, Galileo=2, BeiDou=3, IMES=4, QZSS=5, GLONASS=6
  uint8_t svId; // GPS=1-32, SBAS=120-158, Galileo=211-246, BeiDou=159-163,33-64, IMES=173-182, QZSS=193-202, GLONASS=65-96,255
  uint8_t satNum; // Adjusted Id to number sequentially 1- for each Id group.
  uint8_t cno; // Carrier to noise ratio
  int8_t elev; // Elevation +/-90
  uint16_t azim; // Azimuth 0-360
  uint32_t flags; // See UBX_NAV_SAT command in U-Blox protocol documentation
};

class ubloxGPS
{
public: // Public API
  ubloxGPS();

  // Initializes the GPS unit and stream you will be using.
  // This call may take a few seconds to run.
  // stream - Pointer to the stream object, typically &Serial1, but could be a SW Serial implementation
  // baud - Desired baud rate. This can be tricky so I recommend just sticky with the 9600 default.
  //        The trickyness comes from having to issue the commands in whatever baud the GPS unit is
  //        currently configured, which you may bot know, and then switch to the new baud rate.
  // messagePeriodMs - Should be 1000, 500, 250 which would be 1, 2, or 4Hz
  // configureUBLOX - true means to reconfigure the UBLOX chip. The configuration is saved through
  //                   sleep and wake cycles, so you don't need to reconfigure it except through
  //                   power cycles.
  // Returns: true if succeeded, false if it fails, see lastError for a string description of the problem
  bool init(Stream *stream, uint16_t baud, uint16_t messagePeriodMs, bool configureUBLOX);

  // MUST BE CALLED on entry to loop() from main program, or call it from Serial1.serialEvent()
  // This function takes the data off the Stream connected to the GPS chip and processes it.
  // Note when the GPS time is first by the U-Blox chip, this method will call
  // Time.setTime(), setting the system time is set to the GPS clock time. This will
  // cause the time to jump, so take care in your main loop to account for this if you
  // have anything counting on time progressing smoothly.
  void loop();

  // sleep the GPSunit for the given milliseconds, saves power.
  // With the GPS running, my experiance has the CAM-M8 taking about 25mA
  // In sleep mode it takes less than 1mA, nearly unmeasurable on my crude equipment.
  // Note: your hardware setup may vary considerably if you have a powered antenna, etc...
  // sleepPeriodMS - Number of ms to sleep. Note: If you sleep more than a minute or so, when you
  //                 awaken it may take a while to reacquire a fix.
  // wakeOnRX - If true, then sending any command to the ublox will wake it on command.
  void sleep(uint32_t sleepPeriodMS, bool wakeOnRX=true);

  // Print debugging stream to Serial
  void setDebuggingOutput(bool f) { debuggingOutput = f; ubx.setDebuggingOutput(f);};
  bool getDebuggingOutput() { return debuggingOutput;};

  // Cause any existing bytes in the Stream input buffer to be consumed and discarded.
  void flushStream(Stream *s, uint16_t baud);

// Data is all public, so rather than providing accessors, you can just grab what you want
// Don't change the data
public:
  FixValidity fixValid; // Do we have a fix
  bool timeValid; // Is the time stored in Time.now() valid.
  double latitudeDegrees, longitudeDegrees; // In degrees
  double altitudeMeters; // In meters
  double headingDegrees; // In degrees
  double overGroundSpeedMetersPerSecond, climbSpeedMetersPerSecond; // In meters/second
  double positionAccuracyMeters, altitudeAccuracyMeters; // In Meters
  uint8_t numSatellitesUsed; // Number of satellites used in the fix
  time_t timeDataWasAcquired; // Time data the last data were acquired (Time.now())
  int32_t lastPollTimeJump; // How much the system time jumped due to the last successful loop() call

  static const uint8_t MAX_SATELLITES = 32; // Most tracking channels available in any M8 hardware as of 2020
  SatelliteInfo sats[MAX_SATELLITES];
  uint8_t numberOfSatellites;


private:
  Stream *s;
  ublox_protocol ubx;
  bool debuggingOutput;
  void gotNAV_PVT();
  void gotNAV_SAT();
public:
  void messageComplete(uint8_t classCode, uint8_t idCode);
};
