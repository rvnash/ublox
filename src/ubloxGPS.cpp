/*
  ubloxGPS - Simplified interface to U-Blox M8 style GPS system

  The key feature is this turns off the NMEA style GPS and uses the
  smaller and more efficient UBX messages. The interface is simplified
  to a few commands to init, pump from the loop(), and grab results
  directly from public variables.

  Copyright (c) 2020 - Richard Nash
*/

#include "ubloxGPS.h"
#include <Particle.h>

static ubloxGPS *singleton = NULL;

void messageCompleteFunc(uint8_t classCode, uint8_t idCode)
{
  if (singleton) singleton->messageComplete(classCode,idCode);
}

// Public API
ubloxGPS::ubloxGPS()
{
  singleton = this;
  fixValid = FixValidity::FixNo;
  numberOfSatellites = 0;
  debuggingOutput = false;
  s = NULL;
}

bool ubloxGPS::init(Stream *stream, uint16_t baud, uint16_t messagePeriodMs, bool configureUBLOX)
{
  s = stream;
  uint8_t status;

  if (s == &Serial1) {
    Serial1.begin(baud,SERIAL_8N1);
  }

  if (configureUBLOX) {
    uint8_t retries = 3;
    status = 1;
    // Try configuring the serial port 3 times
    // occasionally it takes some retries to clear it up, not sure why
    while (status && retries > 0) {
      flushStream(s,baud);
      ubx.begin_send(ublox_protocol::UBX_CFG_PRT); // Configure the Serial port
      ubx.put_U1(1); // portID
      ubx.put_U1(0); // reserved 1
      ubx.put_X2(0); // txReady
      ubx.put_X4(0x000008d0); // mode
      ubx.put_U4(baud); // baud rate
      ubx.put_X2(1); // inProtoMask - UBX protocol only
      ubx.put_X2(1); // outProtoMask - UBX protocol only
      ubx.put_X2(0); // flags
      ubx.put_U2(0); // reserved2
      ubx.end_send(s);
      status = ubx.getPollResponse(ublox_protocol::CLASS_ACK,1,s,1200);
      retries--;
      if (status && retries > 0) {
        if (debuggingOutput) {
          Serial.printf("Could not configure Serial Port, retrying.\n");
        }
      }
    }

    if (status != 0) {
      if (debuggingOutput) {
        Serial.printf("Could not configure Serial Port.\n");
      }
      return false;
    }


    flushStream(s,baud);
    // Turn on the UBX messages
    ubx.begin_send(ublox_protocol::UBX_CFG_MSG);
    ubx.put_U1(ublox_protocol::CLASS_NAV); // Message class NAV
    ubx.put_U1(0x07); // Message ID PVT
    ubx.put_U1(1); // Rate on current port
    ubx.end_send(s);
    status = ubx.getPollResponse(ublox_protocol::CLASS_ACK,1,s,1200);

    if (status != 0) {
      if (debuggingOutput) {
        Serial.printf("Could not enable NAV-PVT message.\n");
      }
      return false;
    }

    flushStream(s,baud);
    ubx.begin_send(ublox_protocol::UBX_CFG_MSG);
    ubx.put_U1(ublox_protocol::CLASS_NAV); // Message class NAV
    ubx.put_U1(0x35); // Message ID SAT
    ubx.put_U1(1); // Rate on current port
    ubx.end_send(s);
    status = ubx.getPollResponse(ublox_protocol::CLASS_ACK,1,s,1200);

    if (status != 0) {
      if (debuggingOutput) {
        Serial.printf("Could not enable NAV-SAT message.\n");
      }
      return false;
    }

    flushStream(s,baud);
    // Set solution rate
    ubx.begin_send(ublox_protocol::UBX_CFG_RATE);
    ubx.put_U2(messagePeriodMs); // Period
    ubx.put_U2(1); // navRate
    ubx.put_U2(1); // timeRef (GPS)
    ubx.end_send(s);
    status = ubx.getPollResponse(ublox_protocol::CLASS_ACK,1,s,1200);

    if (status != 0) {
      if (debuggingOutput) {
        Serial.printf("Could not set solution rate.\n");
      }
      return false;
    }

    flushStream(s,baud);
    // PMS setting "Super-E" power savings
    ubx.begin_send(ublox_protocol::UBX_CFG_PMS);
    ubx.put_U1(0); // version
    ubx.put_U1(0x03); // Aggressive 1Hz power saving
    ubx.put_U2(0); // period
    ubx.put_U2(0); // onTime
    ubx.put_U1(0); // reserved1
    ubx.put_U1(0); // reserved1
    ubx.end_send(s);
    status = ubx.getPollResponse(ublox_protocol::CLASS_ACK,1,s,1200);

    if (status != 0) {
      if (debuggingOutput) {
        Serial.printf("Could not set power mode to Super-E.\n");
      }
      return false;
    }

    flushStream(s,baud);
    // Save the configuration to battery backed ram so it comes back up like this after sleep
    // However, the configuration is not saved through power cycles.
    ubx.begin_send(ublox_protocol::UBX_CFG_CFG);
    ubx.put_X4(0x00); // Clear Mask
    ubx.put_X4(0x0B); // Save Mask - PRT, MSG, and NAV configurations
    ubx.put_X4(0x00); // Load Mask
    ubx.put_X1(0x01); // Save in Battery Backed Ram - preseved accross sleep, but will reset on power cycle
    ubx.end_send(s);
    status = ubx.getPollResponse(ublox_protocol::CLASS_ACK,1,s,1200);

    if (status != 0) {
      if (debuggingOutput) {
        Serial.printf("CFG save failed.\n");
      }
      return false;
    }
  }
  ubx.setMessageCallback(messageCompleteFunc);
  return true;
}

void ubloxGPS::loop()
{
  while (s && s->available()) ubx.serialGotByte(s->read());
}

void ubloxGPS::sleep(uint32_t sleepPeriodMS, bool wakeOnRX)
{
  if (!s) return;
  ubx.begin_send(ublox_protocol::UBX_RXM_PMREQ);
  ubx.put_U1(0x00); // This mesage version 0x00
  ubx.put_U1(0x00); // reserved1 (3 bytes)
  ubx.put_U1(0x00);
  ubx.put_U1(0x00);
  ubx.put_U4(sleepPeriodMS); // duration
  ubx.put_X4(0x2); // flags  (backup)
  if (wakeOnRX)
    ubx.put_X4(0x08); // flags RX
  else
    ubx.put_X4(0x00); // flags nothing
  ubx.end_send(s);
  // Doesn't poll for any response
}

// Private functions
void ubloxGPS::gotNAV_PVT()
{
  // Gather the entire 92 byte structure into local variables
  uint32_t iTOW = ubx.get_U4();
  uint16_t year = ubx.get_U2();
  uint8_t month = ubx.get_U1();
  uint8_t day = ubx.get_U1();
  uint8_t hour = ubx.get_U1();
  uint8_t min = ubx.get_U1();
  uint8_t sec = ubx.get_U1();
  uint8_t valid = ubx.get_X1();
  uint32_t tAcc = ubx.get_U4();
  int32_t nano = ubx.get_I4();
  uint8_t fixType = ubx.get_U1();
  uint8_t flags = ubx.get_X1();
  uint8_t flags2 = ubx.get_X1();
  uint8_t numSV = ubx.get_U1();
  int32_t lon = ubx.get_I4();
  int32_t lat = ubx.get_I4();
  int32_t height = ubx.get_I4();
  int32_t hMSL = ubx.get_I4();
  uint32_t hAcc = ubx.get_U4();
  uint32_t vAcc = ubx.get_U4();
  int32_t velN = ubx.get_I4();
  int32_t velE = ubx.get_I4();
  int32_t velD = ubx.get_I4();
  int32_t gSpeed = ubx.get_I4();
  int32_t headMot = ubx.get_I4();
  uint32_t sAcc = ubx.get_U4();
  uint32_t headAcc = ubx.get_U4();
  uint16_t pDOP = ubx.get_U2();
  uint8_t flags3 = ubx.get_X1();
  // Five reserved1 bytes
  ubx.get_U1();
  ubx.get_U1();
  ubx.get_U1();
  ubx.get_U1();
  ubx.get_U1();
  int32_t headVeh = ubx.get_I4();
  int16_t magDec = ubx.get_I2();
  uint16_t magAcc = ubx.get_U2();

  // Fill in the instance variables
  switch (fixType) {
    case 0: // No
      fixValid = FixNo;
    break;
    case 1: // dead reckonning
      fixValid = Fix2D;
    break;
    case 2: // 2D
      fixValid = Fix2D;
    break;
    case 3: // 3D
      fixValid = Fix3D;
    break;
    case 4: // GNSS and dead reckonning
      fixValid = Fix3D;
    break;
    case 5: // Time only
      fixValid = FixNo;
    break;
  }
  timeValid = (valid & 0x4) ? true : false;
  latitudeDegrees = (double)lat * 1E-7;
  longitudeDegrees = (double)lon * 1E-7;
  altitudeMeters = (double)hMSL * 0.001;
  headingDegrees = (double)headMot * 1E-5;
  overGroundSpeedMetersPerSecond = (double)gSpeed * 0.001;
  climbSpeedMetersPerSecond = -(double)velD * 0.001;
  positionAccuracyMeters = (double)hAcc * 0.001;
  altitudeAccuracyMeters = (double)vAcc * 0.001;
  numSatellitesUsed = numSV;
  lastPollTimeJump = 0;
  if (timeValid) {
    struct tm t;
    t.tm_year = year-1900;
    t.tm_mon = month - 1;
    t.tm_mday = day;
    t.tm_hour = hour;
    t.tm_min = min;
    t.tm_sec = sec;
    t.tm_isdst = 0;  // not used
    time_t now = Time.now();
    time_t newNow = mktime(&t);
    if (!Time.isValid() || abs((int32_t)(now-newNow)) > 20) { // Only update the time if it jumps more than 20 seconds
      Time.setTime(newNow); // Causes Time.isValid() to return true
      lastPollTimeJump = (int32_t)(newNow - now);
    }
  }
  timeDataWasAcquired = Time.now();
}

void ubloxGPS::gotNAV_SAT()
{
  uint32_t iTOW = ubx.get_U4();
  uint8_t version = ubx.get_U1();
  numberOfSatellites = ubx.get_U1();
  if (numberOfSatellites > MAX_SATELLITES) {
    if (debuggingOutput) {
      Serial.printf("Satellites overflow\n");
    }
    numberOfSatellites = MAX_SATELLITES;
  }
  ubx.get_U1(); // reserve1 2 bytes
  ubx.get_U1();
  for (uint8_t i = 0; i < numberOfSatellites; i++) {
    sats[i].gnssId = ubx.get_U1();
    sats[i].svId = ubx.get_U1();
    switch (sats[i].gnssId) {
      case 0: // GPS
        sats[i].satNum = sats[i].svId;
      break;
      case 1: // SBAS
        sats[i].satNum = sats[i].svId - 119;
      break;
      case 2: // Galileo
        sats[i].satNum = sats[i].svId - 210;
      break;
      case 3: // BeiDou
        if (sats[i].svId >= 159)
          sats[i].satNum = sats[i].svId - 158;
        else
          sats[i].satNum = sats[i].svId - 27;
      break;
      case 4: // IMES
        sats[i].satNum = sats[i].svId - 172;
      break;
      case 5: // QZSS
        sats[i].satNum = sats[i].svId - 192;
      break;
      case 6: // GLONASS
        if (sats[i].svId >= 65) {
          sats[i].satNum = sats[i].svId - 64;
        } else {
          sats[i].satNum = sats[i].svId;
        }
      break;
    }
    sats[i].cno = ubx.get_U1();
    sats[i].elev = ubx.get_I1();
    sats[i].azim  = ubx.get_I2();
    ubx.get_I2(); // "Psuedorange residual" whatever that is.
    sats[i].flags = ubx.get_X4();
  }
}

// Wait until the Stream is clear for a characters worth of time
void ubloxGPS::flushStream(Stream *s, uint16_t baud)
{
  uint32_t bytesEaten = 0;
  uint32_t timeoutMS=millis();
  // How long to wait between characters, for 9600 baud or more this is just 2ms
  // 8-bits, 1 stop bit
  uint32_t periodMS = (9000 / baud) + 2;
  while (millis()-timeoutMS < periodMS) {
    while (s->available()) {
      s->read();
      bytesEaten++;
      timeoutMS = millis();
    }
  }
  if (debuggingOutput) {
    Serial.printf("Flush stream ate %d bytes\n", bytesEaten);
  }
}

void ubloxGPS::messageComplete(uint8_t classCode, uint8_t idCode)
{
  if (classCode == ublox_protocol::CLASS_NAV && idCode == 0x07 ) { // NAV-PVT
    gotNAV_PVT();
  } else if (classCode == ublox_protocol::CLASS_NAV && idCode == 0x35 ) {
    gotNAV_SAT();
  } else {
    if (debuggingOutput) {
      Serial.printf("Got unknown message %d/%d\n", classCode, idCode);
    }
  }
}
