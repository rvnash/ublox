#pragma once

/*
uBlox - Simple class to help configure and manage uBlox M8 GPS units
Richard Nash 2020

See: https://www.u-blox.com/en/docs/UBX-13003221
*/

#include "application.h"
#include <stdlib.h>

enum UBXMessageState {
  // VALID, EMPTY, BADCHECKSUM, and BUFFER_OVERFLOW are terminal states,
  // meaning the state machine will start looking for the start of a new
  // message.
  VALID,
  EMPTY,
  BADCHECKSUM,
  BUFFER_OVERFLOW,
  // These states define what the state machine is looking for next
  GETTING_HEADER_62,
  GETTING_CLASS,
  GETTING_ID,
  GETTING_LENGTH,
  GETTING_PAYLOAD,
  GETTING_CHECKSUM
};

class ublox_protocol
{
public:
  // Classes, comprehensive UBX list
  static const uint8_t CLASS_NULL = 0x00; // Not a valid UBX message class
  static const uint8_t CLASS_NAV = 0x01;
  static const uint8_t CLASS_RXM = 0x02;
  static const uint8_t CLASS_INF = 0x04;
  static const uint8_t CLASS_ACK = 0x05;
  static const uint8_t CLASS_CFG = 0x06;
  static const uint8_t CLASS_UPD = 0x09;
  static const uint8_t CLASS_MON = 0x0A;
  static const uint8_t CLASS_AID = 0x0B;
  static const uint8_t CLASS_TIM = 0x0D;
  static const uint8_t CLASS_ESF = 0x10;
  static const uint8_t CLASS_MGA = 0x13;
  static const uint8_t CLASS_LOG = 0x21;
  static const uint8_t CLASS_SEC = 0x27;
  static const uint8_t CLASS_HNR = 0x28;

  // Send these to ublox::begin().
  // Little helper define here
#define UBLOX_MSG(msg_class,id) (uint16_t)(((uint16_t)(msg_class))<<8) | (uint16_t)(id)

  // Specify the frequency of messages per measurement for each message type.
  static const uint16_t UBX_CFG_MSG = UBLOX_MSG(CLASS_CFG, 0x01);

  // Clear, save, and load configurations from storage
  static const uint16_t UBX_CFG_CFG = UBLOX_MSG(CLASS_CFG, 0x09);

  // Port configuration for UARTS
  static const uint16_t UBX_CFG_PRT = UBLOX_MSG(CLASS_CFG, 0x00);

  // Get the current navigational solution
  static const uint16_t UBX_NAV_PVT = UBLOX_MSG(CLASS_NAV, 0x07);

  // Get the current Satellite information
  static const uint16_t UBX_NAV_SAT = UBLOX_MSG(CLASS_NAV, 0x35);

  // Put the receiver in a defined power state
  static const uint16_t UBX_CFG_PWR = UBLOX_MSG(CLASS_CFG, 0x57);

  // Navigation/measurement rate settings
  static const uint16_t UBX_CFG_RATE = UBLOX_MSG(CLASS_CFG, 0x08);

  // Reset the reciever
  static const uint16_t UBX_CFG_RST = UBLOX_MSG(CLASS_CFG, 0x04);

  // Configuration for Antenna
  static const uint16_t UBX_CFG_ANT = UBLOX_MSG(CLASS_CFG, 0x13);

  // Configuration Power mode
  static const uint16_t UBX_CFG_PMS = UBLOX_MSG(CLASS_CFG, 0x86);

  // Request software version information
  static const uint16_t UBX_MON_VER = UBLOX_MSG(CLASS_MON, 0x04);

  // Request software version information
  static const uint16_t UBX_MON_HW = UBLOX_MSG(CLASS_MON, 0x09);

  // Power sleeping command
  static const uint16_t UBX_RXM_PMREQ = UBLOX_MSG(CLASS_RXM, 0x41);

public:
  ublox_protocol();

  // These needs to be pumpled with characters to drive the parsing engine
  void serialGotByte(uint8_t b);

// Functions for sending commands
  void begin_send(uint16_t msg); // Start a message sequence. CLEARS any recieved message
  // Each of these returns true on success, false if output buffer is full
  bool put_U1(uint8_t x);
  bool put_RU1_3(uint8_t x);
  bool put_I1(int8_t x);
  bool put_X1(uint8_t x);
  bool put_U2(uint16_t x);
  bool put_I2(int16_t x);
  bool put_X2(uint16_t x);
  bool put_U4(uint32_t x);
  bool put_I4(int32_t x);
  bool put_X4(uint32_t x);
  bool put_R4(float x);
  bool put_R8(double x);
  bool put_CH(char c);
  void end_send(Stream *s); // Sends the command

// Functions for recieving results
  void setMessageCallback(void (*messageCompleteFunc)(uint8_t,uint8_t));

  // Operates in one of 3 modes
  // Mode 1: pClassCode == ACK
  //         Waiting for ACK-ACK - Will return 0 on ACK, or 3 if NACK comes instead.
  //         Callback function gets called on any other message
  // Mode 2: pClassCode != CLASS NULL
  //         Waiting for a specific message
  //         Will return 0 when that specific Class/ID message comes in.
  //         Callback function gets called on any other message
  // Mode 3: pClassCode == CLASS NULL
  //         Waiting for any message
  //         Will return 0 when any message comes in, after calling callBack
  // Returns
  // 0 - Success
  // 1 - Timeout waiting for this response
  // 2 - Message had checksum error
  // 3 - Waiting for ACK-ACK, got ACK-NACK
  // 4 - Buffer overlflowed (MAX_PAYLOAD_LENGTH bytes)
  uint8_t getPollResponse(uint8_t pclassCode, uint8_t pidCode, Stream *s, uint32_t timeoutMS=1000);

  // Current state of the last message
  // Each of these will return 0 if out of buffer, but that's not really legit error throwing
  UBXMessageState getMessageState();
  uint32_t getMessageTime();
  uint8_t  getClass();
  uint8_t  getID();
  uint16_t getPayloadLength();
  // The following progress through the payload one message unit at a time
  uint8_t  get_U1();
  uint8_t  get_RU1_3();
  int8_t   get_I1();
  uint8_t  get_X1();
  uint16_t get_U2();
  int16_t  get_I2();
  uint16_t get_X2();
  uint32_t get_U4();
  int32_t  get_I4();
  uint32_t get_X4();
  float    get_R4();
  double   get_R8();
  char     get_CH();
  uint8_t *get_Buffer(uint16_t size);

  // Debugging debugging debugging output
  void setDebuggingOutput(bool f) { debuggingOutput = f;};

private:
  void (*messageCompleteFunc)(uint8_t,uint8_t);

  static const uint16_t MAX_PAYLOAD_LENGTH = 512;
  UBXMessageState messageState;
  uint32_t messageTime; // In millis()
  uint8_t payload[MAX_PAYLOAD_LENGTH]; // Used for both input and output
  uint8_t classCode, idCode;
  uint16_t payloadLength;
  uint16_t payloadReadingPosition;
  uint8_t CK_A, CK_B;
  bool debuggingOutput;

  void bout(Stream *s,uint8_t b);
  void init_checksum();
  void checksumByte(uint8_t b);
  void bin(uint8_t b, bool inPollingLoop=false);
  void callBackWithValidMessage();
};
