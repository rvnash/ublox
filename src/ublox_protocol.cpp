/*
ublox - Class for intilzing and controlling a ublox GPS device
Richard Nash 2020
*/

#include "ublox_protocol.h"

ublox_protocol::ublox_protocol()
{
  payloadLength = payloadReadingPosition = 0;
  messageState = UBXMessageState::EMPTY;
  messageCompleteFunc = NULL;
  debuggingOutput = false;
}

void ublox_protocol::serialGotByte(uint8_t b)
{
  bin(b);
}

void ublox_protocol::begin_send(uint16_t msg)
{
  messageState = UBXMessageState::EMPTY;
  payloadLength = payloadReadingPosition = 0;
  classCode = msg >> 8;
  idCode = msg & 0xff;
}

void ublox_protocol::end_send(Stream *s)
{
  init_checksum();
  bout(s,0xB5);
  bout(s,0x62);
  bout(s,classCode);            checksumByte(classCode);
  bout(s,idCode);               checksumByte(idCode);
  bout(s,payloadLength & 0xff); checksumByte(payloadLength & 0xff);
  bout(s,payloadLength >> 8);   checksumByte(payloadLength >> 8);

  if (debuggingOutput) {
    Serial.printf("Send: B5 62 %02X %02X (%d) | ", classCode, idCode, payloadLength);
  }

  for (uint8_t i = 0; i < payloadLength; ++i)
  {
    bout(s,payload[i]);
    checksumByte(payload[i]);
    if (debuggingOutput) {
      Serial.printf("%02X ", payload[i]);
    }
  }
  bout(s,CK_A);
  bout(s,CK_B);
  if (debuggingOutput) {
    Serial.printf("| %02X %02X\n", CK_A, CK_B);
  }
  s->flush();

  messageState = UBXMessageState::EMPTY;

  // if this was a UART Baud Rate change, the result should come
  // back as the new baud rate.
  if (&Serial1 == s && classCode == CLASS_CFG && idCode == 0x00 && payloadLength == 20) { // This was a set UART call, reset baud rate
    uint32_t *baud = (uint32_t *)(payload+8);
    if (debuggingOutput) {
      Serial.printf(" Setting baud to: %d\n", *baud);
    }
    Serial1.begin(*baud,SERIAL_8N1);
  }
}

bool ublox_protocol::put_U1(uint8_t x)
{
  if (payloadLength >= MAX_PAYLOAD_LENGTH) return false;
  payload[payloadLength++] = x;
  return true;
}

bool ublox_protocol::put_RU1_3(uint8_t x)
{
  return put_U1(x);
}

bool ublox_protocol::put_I1(int8_t x)
{
  return put_U1((uint8_t)x);
}

bool ublox_protocol::put_X1(uint8_t x)
{
  return put_U1(x);
}

bool ublox_protocol::put_U2(uint16_t x)
{
  // Litle endian
  if (!put_U1(x&0xff)) return false;
  return put_U1(x>>8);
}

bool ublox_protocol::put_I2(int16_t x)
{
  return put_U2((uint16_t)x);
}

bool ublox_protocol::put_X2(uint16_t x)
{
  return put_U2((uint16_t)x);
}

bool ublox_protocol::put_U4(uint32_t x)
{
  // Litle endian
  if (!put_U1(x&0xff)) return false;
  if (!put_U1((x&0xff00)>>8)) return false;
  if (!put_U1((x&0xff0000)>>16)) return false;
  return put_U1((x&0xff000000)>>24);
}

bool ublox_protocol::put_I4(int32_t x)
{
  return put_U4((uint32_t)x);
}

bool ublox_protocol::put_X4(uint32_t x)
{
  return put_U4((uint32_t)x);
}

bool ublox_protocol::put_R4(float x)
{
  uint32_t *i = (uint32_t *)&x;
  return put_U4(*i);
}

bool ublox_protocol::put_R8(double x)
{
  uint64_t *i = (uint64_t *)&x;

  // Litle endian
  if (!put_U1((*i)&0xff)) return false;
  if (!put_U1(((*i)&0xff00)>>8)) return false;
  if (!put_U1(((*i)&0xff0000)>>16)) return false;
  if (!put_U1(((*i)&0xff000000)>>24)) return false;
  if (!put_U1(((*i)&0xff00000000)>>32)) return false;
  if (!put_U1(((*i)&0xff0000000000)>>40)) return false;
  if (!put_U1(((*i)&0xff000000000000)>>48)) return false;
  return put_U1(((*i)&0xff00000000000000)>>56);
}

bool ublox_protocol::put_CH(char c)
{
  return put_U1((uint8_t)c);
}

void ublox_protocol::setMessageCallback(void (*messageCompleteFunc)(uint8_t,uint8_t))
{
  this->messageCompleteFunc = messageCompleteFunc;
}

uint8_t ublox_protocol::getPollResponse(uint8_t pclassCode, uint8_t pidCode, Stream *s, uint32_t timeoutMS)
{
  uint32_t start = millis();
  if (debuggingOutput) {
    Serial.printf("Polling for %d/%d,T:%d : ", pclassCode, pidCode,timeoutMS);
  }
  while (millis()-start < timeoutMS) {
    if (s->available()) {
      uint8_t b = s->read();
      bin(b,true); // Tell bin to not call back
      if (messageState == UBXMessageState::VALID) {
        if (pclassCode == CLASS_NULL) { // Waiting for any message
          callBackWithValidMessage();
          return 0; // Success
        } else if (pclassCode == CLASS_ACK && pclassCode == getClass()) { // Waiting for ACK OR NACK
          if (getID() == 0x01) {  // Got ACK
            return 0; // Success
          } else {
            return 3; // Got NACK
          }
        } else if (pclassCode == getClass() && pidCode == getID()) {
          // Got the message I was wating for (NO CALLBACK)
          return 0; // Success
        } else {
          // Got a message I was not waiting for
          callBackWithValidMessage();
          // Continue looking for this message
        }
      } else if (messageState == UBXMessageState::BADCHECKSUM) {
        return 2;
      } else if (messageState == UBXMessageState::BUFFER_OVERFLOW) {
        return 4;
      }
    }
  }
  return 1; // Timeout waiting
}

UBXMessageState ublox_protocol::getMessageState()
{
  return messageState;
}

uint32_t ublox_protocol::getMessageTime()
{
  return messageTime;
}

uint8_t ublox_protocol::getClass()
{
  return classCode;
}

uint8_t ublox_protocol::getID()
{
  return idCode;
}

uint16_t ublox_protocol::getPayloadLength()
{
  return payloadLength;
}

uint8_t ublox_protocol::get_U1()
{
  if (payloadReadingPosition < payloadLength) {
    return payload[payloadReadingPosition++];
  } else {
    return 0;
  }
}

uint8_t ublox_protocol::get_RU1_3()
{
  return get_U1();
}

int8_t ublox_protocol::get_I1()
{
  return (int8_t)get_U1();
}

uint8_t ublox_protocol::get_X1()
{
  return get_U1();
}

uint16_t ublox_protocol::get_U2()
{
  if (payloadReadingPosition + 2 >= payloadLength) {
    return 0;
  }
  payloadReadingPosition += 2;
  return ((uint16_t)payload[payloadReadingPosition-1]<<8) | payload[payloadReadingPosition-2];
}

int16_t ublox_protocol::get_I2()
{
  return (int16_t)get_U2();
}

uint16_t ublox_protocol::get_X2()
{
  return get_U2();
}

uint32_t ublox_protocol::get_U4()
{
  if (payloadReadingPosition + 4 >= payloadLength) {
    return 0;
  }
  payloadReadingPosition += 4;
  return ((uint32_t)payload[payloadReadingPosition-1]<<24) |
         ((uint32_t)payload[payloadReadingPosition-2]<<16) |
         ((uint32_t)payload[payloadReadingPosition-3]<<8) |
         payload[payloadReadingPosition-4];
}

int32_t ublox_protocol::get_I4()
{
  return (int32_t)get_U4();
}

uint32_t ublox_protocol::get_X4()
{
  return get_U4();
}

float ublox_protocol::get_R4()
{
  union {
    float f;
    uint32_t i;
  } u;
  u.i = get_U4();
  return u.f;
}

double ublox_protocol::get_R8()
{
  union {
    double f;
    uint64_t i;
  } u;
  u.i = (uint64_t)get_U4() | ((uint64_t)get_U4() << 32);
  return u.f;
}

char ublox_protocol::get_CH()
{
  return (char)get_U1();
}

uint8_t *ublox_protocol::get_Buffer(uint16_t size)
{
  if (payloadReadingPosition + size >= payloadLength) {
    return NULL;
  }
  payloadReadingPosition += size;
  return &(payload[payloadReadingPosition-size]);
}

// Private functions

void ublox_protocol::bout(Stream *s, uint8_t b)
{
  s->write(b);
}

void ublox_protocol::init_checksum()
{
  CK_A=0; CK_B=0;
}

void ublox_protocol::checksumByte(uint8_t b)
{
  CK_A = CK_A + b;
  CK_B = CK_B + CK_A;
}

void ublox_protocol::bin(uint8_t b, bool inPollingLoop)
{
  static uint16_t byteCount;
  if (debuggingOutput) {
    Serial.printf("%02X ", b);
  }
  switch (messageState) {
    case UBXMessageState::VALID: // Waiting for B5 ...
    case UBXMessageState::EMPTY:
    case UBXMessageState::BADCHECKSUM:
    case UBXMessageState::BUFFER_OVERFLOW:
      if (b == 0xB5) {
        messageState = UBXMessageState::GETTING_HEADER_62;
        init_checksum();
      } else {
        messageState = UBXMessageState::EMPTY;
      }
      break;
    case UBXMessageState::GETTING_HEADER_62: // Sync Char 2
      if (b == 0x62) {
        messageState = UBXMessageState::GETTING_CLASS;
      } else {
        messageState = UBXMessageState::EMPTY;
      }
      break;
    case UBXMessageState::GETTING_CLASS: // Class
      checksumByte(b);
      classCode = b;
      messageState = UBXMessageState::GETTING_ID;
      break;
    case UBXMessageState::GETTING_ID: // ID
      checksumByte(b);
      idCode = b;
      messageState = UBXMessageState::GETTING_LENGTH;
      byteCount = 0;
      break;
    case UBXMessageState::GETTING_LENGTH: // Payload size
      checksumByte(b);
      if (byteCount == 0) {
        payloadLength = b; // LSB
        byteCount++;
      } else {
        payloadLength = payloadLength | (((uint16_t)b) << 8); // MSB
        if (debuggingOutput) {
          Serial.printf("(%d) | ", payloadLength);
        }
        messageState = UBXMessageState::GETTING_PAYLOAD;
        byteCount = 0;
      }
      break;
    case UBXMessageState::GETTING_PAYLOAD:
      checksumByte(b);
      if (byteCount >= MAX_PAYLOAD_LENGTH) {
        messageState = UBXMessageState::BUFFER_OVERFLOW;
        if (debuggingOutput) {
          Serial.printf(" OVERFLOW\n");
        }
      } else {
        payload[byteCount++] = b;
        if (byteCount == payloadLength) {
          messageState = UBXMessageState::GETTING_CHECKSUM;
          byteCount = 0;
        }
      }
      break;
    case UBXMessageState::GETTING_CHECKSUM: // Read checksum A
      if (byteCount == 0) {
        if (debuggingOutput) {
          Serial.printf("(%02X) ", CK_A);
        }
        byteCount++;
        if (b != CK_A) {
          messageState = UBXMessageState::BADCHECKSUM;
        }
      } else {
        if (debuggingOutput) {
          Serial.printf("(%02X) ", CK_B);
        }
        if (b != CK_B) {
          messageState = UBXMessageState::BADCHECKSUM;
        } else {
          // WE HAVE A VALID MESSAGE!
          if (debuggingOutput) {
            Serial.printf("\n");
          }
          messageState = UBXMessageState::VALID;
          messageTime = millis();
          payloadReadingPosition = 0;
          if (!inPollingLoop) {
            callBackWithValidMessage();
          }
        }
      }
  }
}

void ublox_protocol::callBackWithValidMessage()
{
  if (messageCompleteFunc) {
    messageCompleteFunc(classCode,idCode);
  }
}
